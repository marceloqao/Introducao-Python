# Você está na casa de alguns amigos na qual você passou a noite se divertindo. Vocês estão decidindo quem irá fazer o café da manhã. Vocês decidem fazer o jogo dos dedos, na qual todos os participantes, ao mesmo tempo, decidem quantos dedos irão mostrar, a seguir uma soma de todos os dedos é realizada. Foi decidido que a dona da casa irá contar a partir dela um a um entre os jogadores, onde o número parar, aquela pessoa será a escolhida para fazer o café da manhã.
# Avalie o seguinte código:

mariana = 2 # dona da casa
renato = 4
larissa = 2
rafael = 5
augusto = 1
rafaela = 3
dedos = {mariana, renato, larissa, rafael, augusto, rafaela}
participantes = len(dedos) #quantidade de participantes
somaDedos = sum(dedos)
print(somaDedos)