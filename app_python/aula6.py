## Aula 6 - Organizando Conjuntos e Subconjuntos de elementos em Python
##  - O que é um Conjunto
##  - Métodos de União, Intersecção, Diferença e Diferença Simétrica(aquilo que não tem nos dois conjuntos)
##  - Remoção de duplicidades em listas utilizando conjuntos

conjunto = {1, 2, 3 , 4, 5}
conjunto2 = {5, 6, 7 , 8}
conjunto_uniao = conjunto.union(conjunto2)
print('União: {}'.format(conjunto_uniao))
conjunto_interseccao = conjunto.intersection(conjunto2)
print('Intersecção: {}'.format(conjunto_interseccao))
conjunto_diferenca1 = conjunto.difference(conjunto2)
conjunto_diferenca2 = conjunto2.difference(conjunto)
print('Diferença entre 1 e 2: {}'.format(conjunto_diferenca1))
print('Diferença entre 2 e 1: {}'.format(conjunto_diferenca2))
conjunto_diferenca_simetrica = conjunto.symmetric_difference(conjunto_diferenca2)
print('Diferença simétrica: {}'.format(conjunto_diferenca_simetrica))


conjunto_a = {1, 2, 3}
conjunto_b = {1, 2, 3, 4, 5}
conjunto_subset = conjunto_a.issubset(conjunto_b)
print('A é subconjunto de B ? {}'.format(conjunto_subset))
conjunto_superset = conjunto_b.issuperset(conjunto_a)
print('B é superconjunto de A ? {}'.format(conjunto_superset))

lista = ['cachorro', 'cachorro', 'gato', 'gato', 'elefante']
print(lista)
conjunto_animais = set(lista)
print(conjunto_animais)
lista_animais = list(conjunto_animais)
print(lista_animais)

# conjunto = {1, 2, 3 ,4}
# conjunto.add(5)
# conjunto.discard(2)
# print(conjunto)


# Você está na casa de alguns amigos na qual você passou a noite se divertindo. Vocês estão decidindo quem irá fazer o café da manhã. Vocês decidem fazer o jogo dos dedos, na qual todos os participantes, ao mesmo tempo, decidem quantos dedos irão mostrar, a seguir uma soma de todos os dedos é realizada. Foi decidido que a dona da casa irá contar a partir dela um a um entre os jogadores, onde o número parar, aquela pessoa será a escolhida para fazer o café da manhã.
# Avalie o seguinte código:

mariana = 2 # dona da casa
renato = 4
larissa = 2
rafael = 5
augusto = 1
rafaela = 3

dedos = {mariana, renato, larissa, rafael, augusto, rafaela}
participantes = ______ #quantidade de participantes
somaDedos = ______ #soma dos valores de cada dedo
dedoapontadopara = 0
for x in range(somaDedos):
  if dedoapontadopara > participantes:
     dedoapontadopara = 0
  dedoapontadopara += 1
dedos = _______ #converter dedos para arquivo tipo 'list'
print('No final o dedo foi apontado para {}.'.format(dedos[dedoapontadopara]))

Assinale a alternativa correta com os códigos corretos na sequência das lacunas (a vírgula está separando cada lacuna):
len(dedos) , sum(participantes), list(dedos)

