lista = [12, 10, 5, 7]
lista_animal = ['cachorro', 'gato', 'elefante', 'lobo', 'arara']

# print(lista_animal[1])
# nova_lista =  lista_animal * 3
# print(nova_lista)
# for x in lista_animal:
#     print(x)

# print(sum(lista))
# print(max(lista))
# print(max(lista_animal)) #valor da primeira letra em ascii

# if 'lobo' in lista_animal:
#     print('existe um lobo na lista')
# else:
#     print('não existe um lobo na lista')
#     lista_animal.append('lobo')
#     print(lista_animal)

# lista_animal.pop(0)
# print(lista_animal)

# lista_animal.remove('elefante')
# print(lista_animal)

##Ordenar lista

# lista.sort()
# lista_animal.sort()
# print(lista)
# print(lista_animal)
# lista_animal.reverse()
# print(lista_animal)

##Tuplas: Lista é mutável, enquanto a TUPLA é imutável

# tupla = (1, 10, 12, 14)
# print(tupla)

lista_cores = ['azul', 'amarelo', 'vermelho', 'roxo']
lista_cores.reverse()
print(lista_cores)
